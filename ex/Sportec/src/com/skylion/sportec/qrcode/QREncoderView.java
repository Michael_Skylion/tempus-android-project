package com.skylion.sportec.qrcode;

import com.onbarcode.barcode.android.AndroidColor;
import com.onbarcode.barcode.android.IBarcode;
import com.onbarcode.barcode.android.QRCode;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class QREncoderView extends View {

	private QRCode barcode;

	public QREncoderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		barcode = new QRCode();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		ParseObject sub = (ParseObject) ParseUser.getCurrentUser().get("sub_id");
		try {
			sub.fetchIfNeeded();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		barcode.setData(sub.get("code").toString());
		barcode.setDataMode(QRCode.M_AUTO);
		barcode.setVersion(10);
		barcode.setEcl(QRCode.ECL_M);
		barcode.setFnc1Mode(IBarcode.FNC1_NONE);
		barcode.setProcessTilde(false);
		barcode.setUom(IBarcode.UOM_PIXEL);
		barcode.setX(3f);
		barcode.setLeftMargin(15f);
		barcode.setRightMargin(15f);
		barcode.setTopMargin(15f);
		barcode.setBottomMargin(15f);
		barcode.setResolution(200);
		barcode.setForeColor(AndroidColor.red);
		barcode.setBackColor(AndroidColor.white);
		RectF bounds = new RectF(1, 1, 1, 1);
		try {
			barcode.drawBarcode(canvas, bounds);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
