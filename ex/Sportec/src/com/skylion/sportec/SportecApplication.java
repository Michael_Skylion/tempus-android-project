package com.skylion.sportec;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;

import android.app.Application;

public class SportecApplication extends Application {

	public void onCreate() {
		Parse.initialize(this, "0GCXp2zmKXahSLKlONl6jhNh2re3pK2Ith8bVBNG", "rgEF3Xq4QYnfKTxFJqwBp2LeJKV6zT4hfiXKhSz8");
		PushService.setDefaultPushCallback(this, MainActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();

		ImageLoaderConfiguration config = ImageLoaderConfiguration.createDefault(this);
		ImageLoader.getInstance().init(config);
	}
}
