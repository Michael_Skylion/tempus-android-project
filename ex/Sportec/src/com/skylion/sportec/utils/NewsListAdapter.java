package com.skylion.sportec.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseObject;
import com.skylion.sportec.R;

public class NewsListAdapter extends BaseAdapter {

	private View view;

	private List<ParseObject> itemsList;
	private LayoutInflater inflater = null;

	public NewsListAdapter(Context context, List<ParseObject> itemsList) {
		if (itemsList != null)
			this.itemsList = itemsList;
		else
			this.itemsList = new ArrayList<ParseObject>();
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return itemsList.size();
	}

	@Override
	public Object getItem(int position) {
		return itemsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		view = convertView;
		ParseObject message = itemsList.get(position);
		if (view == null)
			view = inflater.inflate(R.layout.news_item, null);

		TextView title = (TextView) view.findViewById(R.id.newsTitle_itemText);
		TextView date = (TextView) view.findViewById(R.id.newsDate_itemText);
		TextView text = (TextView) view.findViewById(R.id.newsText_itemText);

		title.setText(message.get("title").toString());
		date.setText(message.getCreatedAt().toGMTString());
		text.setText(message.get("text").toString());

		return view;
	}
}
