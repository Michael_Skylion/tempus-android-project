package com.skylion.sportec.parse;

import java.util.List;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.skylion.sportec.R;
import com.skylion.sportec.utils.NewsListAdapter;
import com.skylion.sportec.utils.VisitsListAdapter;
import com.skylion.sportec.views.NewsListActivity;

public class ParseApi {

	public static void setLastNews(final TextView title, final TextView text) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("News");
		query.setLimit(1);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objectList, ParseException e) {
				if (objectList.size() > 0) {
					title.setText(objectList.get(0).getString("title"));
					text.setText(objectList.get(0).getString("text"));
				} else {
				}
			}
		});
	}

	public static void setNewsCount(Context context, TextView countText) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("News");
		try {
			countText.setText(context.getString(R.string.news_more) + " (" + String.valueOf(query.count()) + ")");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public static void setNews(final ListActivity listActivity) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("News");
		query.addDescendingOrder("createdAt");
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objectList, ParseException e) {
				if (objectList.size() > 0) {
					listActivity.setListAdapter(new NewsListAdapter(listActivity, objectList));
				} else {
				}
			}
		});
	}

	public static void setSubscription(final TextView code, final TextView fund) {

		ParseObject sub = (ParseObject) ParseUser.getCurrentUser().get("sub_id");
		try {
			sub.fetchIfNeeded();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		code.setText(sub.get("code").toString());
		fund.setText("$ " + sub.get("funds").toString());
	}

	public static void setVisits(final Context context, final ListView list) {
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setTitle(context.getString(R.string.connection));
		progressDialog.setMessage(context.getString(R.string.connecting_load));
		progressDialog.show();
		ParseObject sub = (ParseObject) ParseUser.getCurrentUser().get("sub_id");
		try {
			sub.fetchIfNeeded();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Visit");
		query.whereEqualTo("sub_id", sub);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objectList, ParseException e) {
				if (e == null) {
					list.setAdapter(new VisitsListAdapter(context, objectList));
					Log.d("message", "Retrieved " + objectList.size() + " messages");
				} else {
					Log.d("message", "Error: " + e.getMessage());
				}
				progressDialog.dismiss();
			}
		});
	}

	public static void setUserInfoByCode(final Context context, String code, final TextView customerName, final TextView fundText,
			final ImageView userPhoto) {
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setTitle(context.getString(R.string.connection));
		progressDialog.setMessage(context.getString(R.string.connecting_load));
		progressDialog.show();
		ParseQuery<ParseUser> query = ParseQuery.getQuery("Subscription");
		query.whereEqualTo("code", Integer.valueOf(code));
		query.include("sun_id");
		query.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> objects, ParseException e) {
				if (e == null && objects.size() > 0) {
					ParseObject sub = objects.get(0);
					fundText.setText(String.valueOf(sub.get("funds")));
					ParseUser user = sub.getParseUser("sub_id");
					setUserInfo(progressDialog, sub, customerName, userPhoto);
				} else {
					Toast.makeText(context, R.string.no_data_found, Toast.LENGTH_SHORT).show();
					progressDialog.dismiss();
				}
			}
		});
	}

	private static void setUserInfo(final ProgressDialog progressDialog, ParseObject sub, final TextView customerName,
			final ImageView userPhoto) {
		final DisplayImageOptions options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher).imageScaleType(ImageScaleType.EXACTLY_STRETCHED).resetViewBeforeLoading(true)
				.cacheInMemory(true).cacheOnDisc(true).displayer(new RoundedBitmapDisplayer(Integer.MAX_VALUE)).build();
		ParseQuery<ParseUser> query = ParseQuery.getQuery("User");
		query.whereEqualTo("sub_id", sub);
		query.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> objects, ParseException e) {
				if (e == null && objects.size() > 0) {
					ParseUser parseUser = objects.get(0);
					customerName.setText(parseUser.getString("username"));
					ImageLoader.getInstance().displayImage(parseUser.getString("photo"), userPhoto, options);
				} else if (e != null)
					e.printStackTrace();
				progressDialog.dismiss();
			}
		});
	}

	public static void setUserVisits(FragmentActivity activity, ListView visitslist) {
		// TODO Auto-generated method stub

	}

	public static void postNews(final Context context, String title, String message) {
		if (!"".equals(title) && !"".equals(message)) {
			final ProgressDialog progressDialog = new ProgressDialog(context);
			progressDialog.setTitle(context.getString(R.string.connection));
			progressDialog.setMessage(context.getString(R.string.connecting_post));
			progressDialog.show();
			ParseObject newsObject = new ParseObject("News");
			newsObject.put("title", title);
			newsObject.put("text", message);
			newsObject.saveInBackground(new SaveCallback() {

				@Override
				public void done(ParseException arg0) {
					progressDialog.dismiss();
					if (arg0 == null) {
						Toast.makeText(context, R.string.post_completed, Toast.LENGTH_SHORT).show();
						context.startActivity(new Intent(context, NewsListActivity.class));
					}
				}
			});
		} else {
			Toast.makeText(context, R.string.empty_fields, Toast.LENGTH_SHORT).show();
		}
	}

}
