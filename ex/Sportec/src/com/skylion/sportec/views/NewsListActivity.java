package com.skylion.sportec.views;

import android.app.ListActivity;
import android.os.Bundle;

import com.skylion.sportec.R;
import com.skylion.sportec.parse.ParseApi;

public class NewsListActivity extends ListActivity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		
		ParseApi.setNews(this);
		
	}
}
