package com.skylion.sportec.views;

import java.util.Random;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.skylion.sportec.R;

public class UserLoginActivity extends ActionBarActivity implements
		OnClickListener, ConnectionCallbacks, OnConnectionFailedListener {

	private ParseUser user;
	private ParseObject subscription;
	private ParseObject visit;

	public final int REQUEST_CODE_RESOLVE_ERR = 9000;
	private static final int RC_SIGN_IN = 123;

	private GoogleApiClient mGoogleApiClient;
	private ProgressDialog progressDialog;

	private boolean mIntentInProgress;

	private boolean mSignInClicked;

	private ConnectionResult mConnectionResult;

	private SignInButton loginButton;
	private Button btnSignOut, btnRevokeAccess;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		progressDialog = new ProgressDialog(this);
		getSupportActionBar().setTitle(R.string.title_activity_auth);

		screenInit();
	}

	private void screenInit() {
		loginButton = (SignInButton) findViewById(R.id.LoginScreen_googleSignInButton);
		btnSignOut = (Button) findViewById(R.id.LoginScreen_googleSignOutButton);
		btnRevokeAccess = (Button) findViewById(R.id.LoginScreen_googleRevokeButton);

		loginButton.setOnClickListener(this);
		btnSignOut.setOnClickListener(this);
		btnRevokeAccess.setOnClickListener(this);

		// Initializing google plus api client
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Plus.API)
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();
	}

	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	/**
	 * Button on click listener
	 * */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.LoginScreen_googleSignInButton:
			// Signin button clicked
			signInWithGplus();
			break;
		case R.id.LoginScreen_googleSignOutButton:
			// Signout button clicked
			signOutFromGplus();
			break;
		case R.id.LoginScreen_googleRevokeButton:
			// Revoke access button clicked
			revokeGplusAccess();
			break;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int responseCode,
			Intent intent) {
		if (requestCode == RC_SIGN_IN) {
			if (responseCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		mSignInClicked = false;
		Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();

		// Get user's information
		getProfileInformation();

		// Update the UI after signin
		updateUI(true);

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();
		updateUI(false);
	}

	/**
	 * Updating the UI, showing/hiding buttons and profile layout
	 * */
	private void updateUI(boolean isSignedIn) {
		if (isSignedIn) {
			loginButton.setVisibility(View.GONE);
			btnSignOut.setVisibility(View.VISIBLE);
			btnRevokeAccess.setVisibility(View.VISIBLE);
		} else {
			loginButton.setVisibility(View.VISIBLE);
			btnSignOut.setVisibility(View.GONE);
			btnRevokeAccess.setVisibility(View.GONE);
		}
	}

	// @Override
	// public void onConnected(Bundle arg0) {
	// if (plusClient != null) {

	// }

	protected void doAuth(final ParseUser user) {
		user.signUpInBackground(new SignUpCallback() {
			public void done(ParseException e) {
				if (e == null) {
					doSubscription(); // the user is new, create wallet for him
				} else {
					signIn(user.getUsername()); // user already registered, sign
												// in
				}
			}
		});
	}

	protected void doVisit() {
		visit = new ParseObject("Visit");
		visit.put("cost", 0);
		visit.put("type", "Initialization");
		visit.put("sub_id", subscription);
		visit.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException arg0) {
				Toast.makeText(UserLoginActivity.this,
						getString(R.string.title_activity_auth),
						Toast.LENGTH_SHORT).show();
				progressDialog.dismiss();
				finish();
			}
		});
	}

	private void doSubscription() {
		subscription = new ParseObject("Subscription");
		subscription.put("funds", 100);
		subscription.put("code", new Random().nextInt(999) * 1000
				+ new Random().nextInt(999));
		subscription.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException arg0) {
				user.put("sub_id", subscription);
				user.saveInBackground(new SaveCallback() {

					@Override
					public void done(ParseException arg0) {
						doVisit();

					}

				});
			}

		});
	}

	protected void signIn(String username) {
		ParseUser.logInInBackground(username, "my-pass", new LogInCallback() {
			public void done(ParseUser user, ParseException e) {
				if (user != null) {
					progressDialog.dismiss();
					Toast.makeText(UserLoginActivity.this,
							getString(R.string.title_activity_auth),
							Toast.LENGTH_SHORT).show();
					finish();
				} else {
					progressDialog.dismiss();
					Toast.makeText(UserLoginActivity.this,
							"Error code: " + e.getMessage(), Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
	}

	/**
	 * Sign-in into google
	 * */
	private void signInWithGplus() {
		if (!mGoogleApiClient.isConnecting()) {
			mSignInClicked = true;
			resolveSignInError();
		}
	}

	/**
	 * Method to resolve any signin errors
	 * */
	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	/**
	 * Sign-out from google
	 * */
	private void signOutFromGplus() {
		if (mGoogleApiClient.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
			updateUI(false);
		}
	}

	/**
	 * Revoking access from google
	 * */
	private void revokeGplusAccess() {
		if (mGoogleApiClient.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
					.setResultCallback(new ResultCallback<Status>() {
						@Override
						public void onResult(Status arg0) {
							Log.e(UserLoginActivity.class.getSimpleName(),
									"User access revoked!");
							mGoogleApiClient.connect();
							updateUI(false);
						}

					});
		}
	}

	//
	// @Override
	// public void onDisconnected() {
	// // Toast.makeText(this, getString(R.string.google_disconnected),
	// // Toast.LENGTH_SHORT).show();
	// }

	@Override
	public void onBackPressed() {
		Intent startMain = new Intent(Intent.ACTION_MAIN);
		startMain.addCategory(Intent.CATEGORY_HOME);
		startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(startMain);
	}

	/**
	 * Fetching user's information name, email, profile pic
	 * */
	private void getProfileInformation() {
		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
				Person currentPerson = Plus.PeopleApi
						.getCurrentPerson(mGoogleApiClient);
				String personName = currentPerson.getDisplayName();
				String personPhotoUrl = currentPerson.getImage().getUrl();
				String personGooglePlusProfile = currentPerson.getUrl();
				String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

				Log.e(UserLoginActivity.class.getSimpleName(), "Name: "
						+ personName + ", plusProfile: "
						+ personGooglePlusProfile + ", email: " + email
						+ ", Image: " + personPhotoUrl);

				user = new ParseUser();
				user.setUsername(personName);
				user.setPassword("my-pass");
				user.setEmail(email);
				user.put("photo", personPhotoUrl);

				doAuth(user);

			} else {
				Toast.makeText(getApplicationContext(),
						"Person information is null", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
