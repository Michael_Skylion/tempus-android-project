package com.skylion.sportec;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;
import com.skylion.sportec.parse.ParseApi;
import com.skylion.sportec.qrcode.QREncoderView;
import com.skylion.sportec.views.NewsListActivity;
import com.skylion.sportec.views.UserLoginActivity;

//import eu.livotov.zxscan.ZXScanHelper;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new UserFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		// MenuItem messageItem = (MenuItem) menu.findItem(R.id.action_message);
		// messageItem.setVisible(false);
		// this.supportInvalidateOptionsMenu();

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			((UserFragment) getSupportFragmentManager().getFragments().get(0))
					.doLoad();
			break;
		case R.id.action_user:
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, new UserFragment()).commit();
			// MenuItem messageItem = (MenuItem)
			// findViewById(R.id.action_message);
			// messageItem.setVisible(false);
			// this.supportInvalidateOptionsMenu();
			break;
		case R.id.action_admin:
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, new AdminFragment()).commit();
			// MenuItem refreshItem = (MenuItem)
			// findViewById(R.id.action_refresh);
			// refreshItem.setVisible(false);
			// this.supportInvalidateOptionsMenu();
			break;
		case R.id.action_message:
			postNewMessage();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void postNewMessage() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle(R.string.post_news_title);
		final View layout = getLayoutInflater().inflate(
				R.layout.post_news_item, null);
		alert.setView(layout);

		alert.setPositiveButton(R.string.post_news_confirm,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						ParseApi.postNews(MainActivity.this, ((EditText) layout
								.findViewById(R.id.postNews_titleText))
								.getText().toString(), ((EditText) layout
								.findViewById(R.id.postNews_messageText))
								.getText().toString());
						dialog.dismiss();
					}
				});

		alert.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
					}
				});

		alert.show();
	}

	public static class UserFragment extends Fragment implements
			OnClickListener {

		TextView codeText;
		TextView fundText;
		TextView newsTitle;
		TextView newsText;
		TextView newsCount;

		ImageView qrCode;
		ListView visitslist;
		LinearLayout newslayout;

		QREncoderView view;

		public UserFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_user, container,
					false);

			view = new QREncoderView(getActivity(), null);

			codeText = (TextView) rootView.findViewById(R.id.codeText);
			fundText = (TextView) rootView.findViewById(R.id.fundsText);
			newsTitle = (TextView) rootView.findViewById(R.id.newsTitle);
			newsText = (TextView) rootView.findViewById(R.id.newsText);
			newsCount = (TextView) rootView.findViewById(R.id.newsCount);

			visitslist = (ListView) rootView.findViewById(R.id.visitsList);
			newslayout = (LinearLayout) rootView.findViewById(R.id.newsLayout);
			newslayout.setOnClickListener(this);

			loadContent();

			return rootView;
		}

		public void loadContent() {
			if (ParseUser.getCurrentUser() == null) {
				startActivityForResult(new Intent(getActivity(),
						UserLoginActivity.class), 0);
			} else {
				doLoad();
			}
		}

		private void doLoad() {
			ParseApi.setLastNews(newsTitle, newsText);
			ParseApi.setSubscription(codeText, fundText);
			ParseApi.setVisits(getActivity(), visitslist);
			ParseApi.setNewsCount(getActivity(), newsCount);
		}

		@Override
		public void onClick(View v) {
			startActivity(new Intent(getActivity(), NewsListActivity.class));
		}
	}

	public static class AdminFragment extends Fragment implements
			OnClickListener {
		final static int SCAN = 1;
		String userCode;

		TextView nameText;
		TextView codeText;
		TextView fundText;

		EditText codeEdit;
		ImageView userPhoto;
		ListView visitslist;

		Button scanButton;
		Button findButton;

		public AdminFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_admin,
					container, false);

			scanButton = (Button) rootView.findViewById(R.id.scanButton);
			scanButton.setOnClickListener(this);

			findButton = (Button) rootView.findViewById(R.id.findButton);
			findButton.setOnClickListener(this);

			nameText = (TextView) rootView.findViewById(R.id.customerName);
			codeText = (TextView) rootView.findViewById(R.id.adminCodeText);
			fundText = (TextView) rootView.findViewById(R.id.adminFundsText);

			codeEdit = (EditText) rootView.findViewById(R.id.codeEdit);
			userPhoto = (ImageView) rootView.findViewById(R.id.userPhoto);
			visitslist = (ListView) rootView.findViewById(R.id.visitsList);

			// ZXScanHelper.setUseExternalApplicationIfAvailable(true);
			// ZXScanHelper.setVibrateOnRead(true);

			loadContent();

			return rootView;
		}

		public void loadContent() {
			if (ParseUser.getCurrentUser() == null) {
				startActivityForResult(new Intent(getActivity(),
						UserLoginActivity.class), 0);
			} else {
				// ZXScanHelper.scan(getActivity(), SCAN);
			}
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.scanButton:
				// ZXScanHelper.scan(getActivity(), SCAN);
				break;
			case R.id.findButton:
				if (!"".equals(codeEdit.getText().toString())) {
					userCode = codeEdit.getText().toString();
					ParseApi.setUserInfoByCode(getActivity(), userCode,
							nameText, fundText, userPhoto);
					codeText.setText(userCode);
				} else {
					Toast.makeText(v.getContext(), R.string.empty_fields,
							Toast.LENGTH_SHORT).show();
				}
				break;
			}
		}

		@Override
		public void onActivityResult(final int requestCode,
				final int resultCode, final Intent data) {
			if (resultCode == RESULT_OK && requestCode == SCAN) {
				// userCode = ZXScanHelper.getScannedCode(data);
				codeText.setText(userCode);
				ParseApi.setUserInfoByCode(getActivity(), userCode, nameText,
						fundText, userPhoto);
			}
		}
	}

}
