package edu.kpi.tempus.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {

	public static String getMessageDetailsData(Date date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd-MM-yyyy KK:mm aa", Locale.getDefault());
		return dateFormat.format(date);
	}
}
