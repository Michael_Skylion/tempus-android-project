package edu.kpi.tempus.elements;

import harker.fitnesseapiwrapper.entities.Subscription;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import edu.kpi.tempus.R;
import edu.kpi.tempus.util.DateFormatter;
import edu.kpi.tempus.util.QREncoderView;

public class SubscriptionAdapter extends ArrayAdapter<Subscription> {

	private Context context;
	private int resourse;

	public SubscriptionAdapter(Context context, int resource, List<Subscription> objects) {
		super(context, resource, objects);

		this.context = context;
		this.resourse = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		Subscription sub = getItem(position);

		if (convertView == null) {

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			convertView = mInflater.inflate(resourse, null);

			holder = new ViewHolder();
			holder.qrCode = new QREncoderView(context, null);

			holder.txtTitle = (TextView) convertView.findViewById(R.id.subscr_item_title);
			holder.txtDate = (TextView) convertView.findViewById(R.id.subscr_item_date);
			holder.txtDebt = (TextView) convertView.findViewById(R.id.subscr_item_debt);
			holder.txtType = (TextView) convertView.findViewById(R.id.subscr_item_type);

			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();

		holder.qrCode.setCode(sub.getQrCode());
		holder.txtDate.setText(DateFormatter.getMessageDetailsData(sub.getAvailableTo()));
		holder.txtTitle.setText(sub.getTitle());
		holder.txtType.setText(sub.getType());
		holder.txtDebt.setText(sub.getDebt().toString());

		return convertView;
	}

	/* private view holder class */
	private class ViewHolder {
		TextView txtTitle;
		TextView txtDate;
		QREncoderView qrCode;
		TextView txtDebt;
		TextView txtType;
	}
}
