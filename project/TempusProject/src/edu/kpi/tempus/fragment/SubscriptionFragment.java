package edu.kpi.tempus.fragment;

import harker.fitnesseapiwrapper.entities.Subscription;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import edu.kpi.tempus.MainActivity;
import edu.kpi.tempus.R;
import edu.kpi.tempus.elements.SubscriptionAdapter;

public class SubscriptionFragment extends Fragment {

	private List<Subscription> list;
	private View rootView;
	private TextView name;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (rootView == null)
			rootView = inflater.inflate(R.layout.subscription_fragment, container, false);
		else {
			// If we are returning from a configuration change:
			// "view" is still attached to the previous view hierarchy
			// so we need to remove it and re-attach it to the current one
			ViewGroup parent = (ViewGroup) rootView.getParent();
			parent.removeView(rootView);
		}
		list = MainActivity.getUser().getSubscriptions();
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
	}

	private void initView() {
		// ((TextView) getView().findViewById(R.id.tab_text)).setText("Tab 1");
		ListView lView = (ListView) rootView.findViewById(R.id.subscr_list);
		lView.setAdapter(new SubscriptionAdapter(getActivity(), R.layout.subscription_item, list));
		name = (TextView) rootView.findViewById(R.id.subscr_name);
		name.setText(MainActivity.getUser().getLastname() + " " + MainActivity.getUser().getFirstname() + " " + MainActivity.getUser().getSecondname());
		lView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			}
		});
	}
}
