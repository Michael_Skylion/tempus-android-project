package edu.kpi.tempus;

import harker.fitnesseapiwrapper.FitnesseAPI;
import harker.fitnesseapiwrapper.entities.User;

import java.util.concurrent.ExecutionException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.widget.EditText;
import edu.kpi.tempus.fragment.NewsFragment;
import edu.kpi.tempus.fragment.SubscriptionFragment;

public class MainActivity extends FragmentActivity {

	private static final String TAB_1_TAG = "tab_1";
	private static final String TAB_2_TAG = "tab_2";
	private static final String TEST_USER_ID = "1";

	private FragmentTabHost mTabHost;
	private String userId = TEST_USER_ID;
	private static User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initView();

		if (userId == null) {
			loginDialog();
		} else {
			testInitServer();
		}
	}

	private void loginDialog() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Enter your phone number");
		final View layout = getLayoutInflater().inflate(
				R.layout.login_dialog_edit, null);
		alert.setView(layout);
		alert.setPositiveButton("Login",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						userId = FitnesseAPI.getUserId(((EditText)layout.findViewById(R.id.editPhone)).getText().toString(), "???").toString();
						dialog.dismiss();
					}
				});

		alert.show();
	}

	public static User getUser() {
		return user;
	}

	private void initView() {
		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

		mTabHost.addTab(mTabHost.newTabSpec(TAB_1_TAG).setIndicator(getString(R.string.subscription_title), null), SubscriptionFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec(TAB_2_TAG).setIndicator(getString(R.string.news_title), null), NewsFragment.class, null);
	}

	private void testInitServer() {
		try {
			user = new UserDetailsTask().execute(userId).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	private class UserDetailsTask extends AsyncTask<String, Void, User> {

		@Override
		protected User doInBackground(String... params) {
			user = FitnesseAPI.getUserDetails(params[0]);
			return user;
		}

	}

}
